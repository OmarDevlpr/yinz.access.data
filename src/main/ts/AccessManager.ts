import {Dao} from "@yinz/commons.data";
import  AuthzGroup  from "../models/AuthzGroup";
import  Profile  from "../models/Profile";
import  User  from "../models/User";
import {Logger, Exception} from "@yinz/commons";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import ProfileAuthz from "main/models/ProfileAuthz";
import AuthzGroupAuthz from "main/models/AuthzGroupAuthz";

export interface AuthzOpts {
    subject: string;
    action: 'execute'| 'create' | 'read'| 'update' | 'delete' | 'any';
}

export interface AccessManagerOptions {    
    logger: Logger;
}

export interface UserAuthz {
    userCode?: string;
    profile?: Profile;
    authzGroup?: AuthzGroup;
    subject?: string;
    action?: string;
}

export default class AccessManager {
    
    public  authzGroupDao: Dao<AuthzGroup>;
    public  profileDao: Dao<Profile>;
    public  userDao: Dao<User>;
    private _logger: Logger;

    constructor(options: AccessManagerOptions ) {
        this._logger = options.logger;
    }

    async lookupUserAuthzes(handler: YinzConnection | YinzTransaction, userCode: string): Promise<User> {

        let user = (await this.userDao.findByFilter(handler, { code: userCode}, {
            user: "__super_user__",
            include: [
                // 1. includ profules
                'profiles',
                // 2. include profiles authzes                
                 'profiles.profileAuthzs', 'profiles.profileAuthzs.authz', 'profiles.authzGroups',
                // 3. include profiles authzGroups,
                'profiles.authzGroups.authzGroupAuthzs', 'profiles.authzGroups.authzGroupAuthzs.authz'
                ]
        }))[0]; // we take first element because code is unique 
                
        if ( !user ) {
            throw new Exception('ER_ACCESS_MNG__LOOKUP_USER_AUTHZES__USER_NOT_FOUND', {
                message: 'The supplied user [' + userCode + '] does not exist!',
                userCode: userCode,
            });
        }            

        return user;
    }


    private isAuthzGranted(requestedAuthz: AuthzOpts, grantedAuthz: ProfileAuthz | AuthzGroupAuthz): AuthzOpts {
                
        let result: any = null;
        
        if ( grantedAuthz.authz.code === requestedAuthz.subject ) { 

            if ( grantedAuthz.allowAll ) {
                result = requestedAuthz;
            }

            else if ( requestedAuthz.action === "any" ) {

                if ( grantedAuthz.allowAll || grantedAuthz.allowCreate || grantedAuthz.allowDelete 
                    || grantedAuthz.allowExecute || grantedAuthz.allowRead || grantedAuthz.allowUpdate) {
                        result = requestedAuthz;
                }
            }

            else {

                const grants = ['execute', 'create', 'read', 'update', 'delete'];
                
                for ( let grant of grants ) {
                    
                    
                    let capGrant = grant.charAt(0).toUpperCase() + grant.slice(1);
                    
                    if ( requestedAuthz.action === grant && grantedAuthz['allow' + capGrant]) {
                        result = requestedAuthz;
                        break;
                    }                    
                }

            }         
        }

        return result;

    }

    async isUserGrantedAuthzes(handler: YinzConnection | YinzTransaction, userCode: string, authz: AuthzOpts): Promise<UserAuthz> {

        let result: any = {};

        if (userCode === "__super_user__") {
            this._logger.debug(`...the user [${userCode}] is granted the requested action on the underlying object as has all grants.`)
            return {
                userCode: userCode,
                subject: "__any__",
                action: "__all__"
            }
        }

        let user = await this.lookupUserAuthzes(handler, userCode);
        
        this._logger.debug('About to lookup authz for user ', JSON.stringify(user))

        // right now we are sure that the user contains all its authz and authzGroups, at this level we have to
        // itterate through all authz and stop whenever we find a match

        let found = false;        
        for ( let profile of user.profiles ) {


            if ( profile.profileAuthzs ) {

                for (let profileAuthz of profile.profileAuthzs ) {
                    if (this.isAuthzGranted(authz, profileAuthz)) {
                        result.subject = authz.subject;
                        result.action = authz.action;
                        result.profile = profile;
                        result.userCode = userCode;
                        found = true;
                        break;
                    }
                }
                if (found) break;                
            }


            if ( profile.authzGroups ) {

                for ( let profileAuthzGroup of profile.authzGroups ) {
                    
                    if (profileAuthzGroup.authzGroupAuthzs ) {

                        for ( let profileAuthzGroupAuthz of profileAuthzGroup.authzGroupAuthzs ) {

                            if (this.isAuthzGranted(authz, profileAuthzGroupAuthz)) {
                                result.subject = authz.subject;
                                result.action = authz.action;
                                result.profile = profile;
                                result.userCode = userCode;
                                found = true;
                                break;
                            }                            
                        }                        
                     }
                    if (found) break;
                }
            }
            if (found) break;
        }

        return Object.keys(result).length === 0 ? null : result;
        
    }

}