
import Container from "@yinz/container/ts/Container";
import AuthzGroup from "../models/AuthzGroup";
import Profile from "../models/Profile";
import User from "../models/User";
import Authz from "../models/Authz";
import AccessManager from "./AccessManager";
import { TypeormDao } from "@yinz/commons.data";
import { Logger } from "@yinz/commons";
import LoginLog from "../models/LoginLog";
import SetupAccess from "../models/SetupAccess";
import SetupAccessLog from "../models/SetupAccessLog";
import ResetAccessLog from "../models/ResetAccessLog";
import ChangePasswordLog from "../models/ChangePasswordLog";
import PrepareResetAccessLog from "../models/PrepareResetAccessLog";
import PrepareSetupAccessLog from "../models/PrepareSetupAccessLog";
import LogoutLog from "../models/LogoutLog";
import AuthzGroupAuthz from "../models/AuthzGroupAuthz";
import ProfileAuthz from "../models/ProfileAuthz";


const registerBeans = (container: Container) => {
    
    let logger = container.getBean<Logger>('logger');    

    let accessManager = new AccessManager({ logger })

    let authzGroupDao = new TypeormDao<AuthzGroup>({model: AuthzGroup, accessManager, logger });
    let authzGroupAuthzDao = new TypeormDao<AuthzGroupAuthz>({model: AuthzGroupAuthz, accessManager, logger});
    let profileAuthzDao = new TypeormDao<ProfileAuthz>({ model: ProfileAuthz, accessManager, logger});
    let authzDao = new TypeormDao<Authz>({model: Authz, accessManager, logger });
    let userDao = new TypeormDao<User>({model: User, accessManager, logger });
    let profileDao = new TypeormDao<Profile>({model: Profile, accessManager, logger });    
    let loginLogDao = new TypeormDao<LoginLog>({ model: LoginLog, accessManager, logger });    
    let logoutLogDao = new TypeormDao<LogoutLog>({ model: LogoutLog, accessManager, logger });    
    let changePasswordLogDao = new TypeormDao<ChangePasswordLog>({ model: ChangePasswordLog, accessManager, logger });    
    let setupAccessDao = new TypeormDao<SetupAccess>({ model: SetupAccess, accessManager, logger });    
    let setupAccessLogDao = new TypeormDao<SetupAccessLog>({ model: SetupAccessLog, accessManager, logger });    
    let resetAccessLogDao = new TypeormDao<ResetAccessLog>({ model: ResetAccessLog, accessManager, logger });    
    let prepareResetAccessLogDao = new TypeormDao<PrepareResetAccessLog>({ model: PrepareResetAccessLog, accessManager, logger });    
    let prepareSetupAccessLogDao = new TypeormDao<PrepareSetupAccessLog>({ model: PrepareSetupAccessLog, accessManager, logger });        

    accessManager.authzGroupDao = authzGroupDao;
    accessManager.profileDao = profileDao;
    accessManager.userDao = userDao;

    container.setBean('authzGroupDao', authzGroupDao);
    container.setBean('authzDao', authzDao);
    container.setBean('userDao', userDao);
    container.setBean('authzGroupAuthzDao', authzGroupAuthzDao);
    container.setBean('profileAuthzDao', profileAuthzDao);
    container.setBean('profileDao', profileDao);
    container.setBean('loginLogDao', loginLogDao);
    container.setBean('logoutLogDao', logoutLogDao);
    container.setBean('changePasswordLogDao', changePasswordLogDao);
    container.setBean('setupAccessDao', setupAccessDao);
    container.setBean('setupAccessLogDao', setupAccessLogDao);
    container.setBean('resetAccessLogDao', resetAccessLogDao);
    container.setBean('prepareResetAccessLogDao', prepareResetAccessLogDao);
    container.setBean('prepareSetupAccessLogDao', prepareSetupAccessLogDao);
    container.setBean('accessManager', accessManager);

};

const registerClazzes = (container: Container) => {

    container.setClazz('AuthzGroup', AuthzGroup);
    container.setClazz('Profile', Profile);
    container.setClazz('AuthzGroupAuthz', AuthzGroupAuthz);
    container.setClazz('ProfileAuthz', ProfileAuthz);
    container.setClazz('User', User);
    container.setClazz('Authz', Authz);
    container.setClazz('LoginLog', LoginLog);
    container.setClazz('LogoutLog', LogoutLog);
    container.setClazz('SetupAccess', SetupAccess);
    container.setClazz('SetupAccessLog', SetupAccessLog);
    container.setClazz('ResetAccessLog', ResetAccessLog);
    container.setClazz('AccessManager', AccessManager);
    container.setClazz('ChangePasswordLog', ChangePasswordLog);
    container.setClazz('PrepareResetAccessLog', PrepareResetAccessLog);
    container.setClazz('PrepareSetupAccessLog', PrepareSetupAccessLog);

};


export {
    registerBeans,
    registerClazzes
};