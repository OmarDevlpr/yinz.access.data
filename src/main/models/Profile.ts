import { Column, Entity, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToMany } from "typeorm";
import { Model } from "@yinz/commons.data";
import  User  from "./User";
import  AuthzGroup  from "./AuthzGroup";
import ProfileAuthz from "./ProfileAuthz";

@Entity()
export default class Profile extends Model {

    /* --- ID --- */
    @PrimaryGeneratedColumn()
    id: number



    /* --- FIELDS --- */
    @Column({ unique: true })
    code: string;

    @Column({ nullable: true })
    name: string;



    /* --- AUDIT TRAIL FIELDS --- */
    @Column({ type: 'date', nullable: true })
    createdOn: Date;

    @Column({ nullable: true })
    createdBy: string;

    @Column({ type: 'date', nullable: true })
    lastUpdateOn: Date;

    @Column({ nullable: true })
    lastUpdateBy: string;



    /* --- ASSOCIATIONS --- */

    @ManyToMany(type => User, user => user.profiles)
    @JoinTable()
    users: User[];

    // @ManyToMany(type => Authz, authz => authz.profiles)
    // @JoinTable()
    // authzs: Authz[];

    @OneToMany(type => ProfileAuthz, profileAuthz => profileAuthz.profile)
    profileAuthzs: ProfileAuthz[];
    
    @ManyToMany(type => AuthzGroup, authGroup => authGroup.profiles)
    @JoinTable()
    authzGroups: AuthzGroup[];

}
