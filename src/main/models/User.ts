import { Column, Entity, PrimaryGeneratedColumn, ManyToMany } from "typeorm";
import { Model } from "@yinz/commons.data";
import  Profile  from "./Profile";

@Entity()
export default class User extends Model {

    /* --- ID --- */
    @PrimaryGeneratedColumn()
    id: number



    /* --- FIELDS --- */
    @Column({unique: true})
    code: string;

    @Column({nullable: true})
    password: string;

    @Column({ nullable: true })
    languageCode: string;

    @Column({ nullable: true })
    wrongAttempts: number;

    @Column({ enum: [ "A", "I", "L"] })
    status: "A" | "I" | "L";

    @Column({ nullable: true, })
    ownerId: number;

    @Column({ nullable: true })
    ownerModelName: string;


    
    /* --- AUDIT TRAIL FIELDS --- */
    @Column({ type: 'date', nullable: true })
    createdOn: Date;

    @Column({ nullable: true })
    createdBy: string;

    @Column({ type: 'date', nullable: true })
    lastUpdateOn: Date;

    @Column({ nullable: true })
    lastUpdateBy: string;



    /* --- ASSOCIATIONS --- */

    @ManyToMany(type => Profile, profile => profile.users)
    profiles: Profile[]
    
}
