import LogModel from "@yinz/commons.data/ts/LogModel";
import { Entity, Column, ManyToOne } from "typeorm";
import User from "./User";

@Entity()
export default class LoginLog extends LogModel {

    @Column({nullable: true})
    reqUser: string;

    @Column({ nullable: true })
    password: string;

    @Column({ nullable: true })
    needToken: boolean;
 
    /* ASSOCIATIONS */
    @ManyToOne(type => User)
    user: User;
}