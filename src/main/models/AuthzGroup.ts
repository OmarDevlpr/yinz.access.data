import { Column, Entity, PrimaryGeneratedColumn, ManyToMany, OneToMany } from "typeorm";
import { Model } from "@yinz/commons.data";
import  Profile  from "./Profile";
import AuthzGroupAuthz from "./AuthzGroupAuthz";

@Entity()
export default class AuthzGroup extends Model {

    /* --- ID --- */
    @PrimaryGeneratedColumn()
    id: number



    /* --- FIELDS --- */
    @Column({ unique: true })
    code: string;

    @Column({ nullable: true })
    name: string;



    /* --- AUDIT TRAIL FIELDS --- */
    @Column({ type: 'date', nullable: true })
    createdOn: Date;

    @Column({ nullable: true })
    createdBy: string;

    @Column({ type: 'date', nullable: true })
    lastUpdateOn: Date;

    @Column({ nullable: true })
    lastUpdateBy: string;



    /* --- ASSOCIATIONS --- */

    @ManyToMany(type => Profile, profile => profile.authzGroups)
    profiles: Profile[];

    // @ManyToMany(type => Authz, authz => authz.authzGroups)
    // @JoinTable()
    // authzs: Authz[];

    @OneToMany(type => AuthzGroupAuthz, authzGroupAuthz => authzGroupAuthz.authzGroup)
    authzGroupAuthzs: AuthzGroupAuthz[];

}
