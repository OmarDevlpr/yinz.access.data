import { Column, Entity, ManyToMany, JoinTable } from "typeorm";
import { Model } from "@yinz/commons.data";
import Profile from "./Profile";


@Entity()
export default class SetupAccess extends Model {
    

    /* --- FIELDS --- */
    @Column({ unique: true })
    code: string;

    @Column()
    password: string;

    @Column({ enum: ["W", "C", "A", "D"] })
    status: "W" | "C" | "A" | "D";

    @Column({ unique: true, nullable: true })
    ownerId: number;

    @Column({ unique: true, nullable: true })
    ownerModelName: string;

    /* ASSOCIATIONS */    
    @ManyToMany(type => Profile)
    @JoinTable()
    profiles: Profile[];
}
