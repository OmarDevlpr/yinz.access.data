import LogModel from "@yinz/commons.data/ts/LogModel";
import { Entity, Column } from "typeorm";

@Entity()
export default class SetupAccessLog extends LogModel {
    
    @Column({ nullable: true })
    password: string;

    @Column({ nullable: true })
    needToken: boolean;
        
}