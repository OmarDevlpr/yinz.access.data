import { Column, Entity, ManyToOne } from "typeorm";
import { Model } from "@yinz/commons.data";
import Authz from "./Authz";
import AuthzGroup from "./AuthzGroup";

@Entity()
export default class AuthzGroupAuthz extends Model {

    @Column({ nullable: true })
    allowCreate: boolean;

    @Column({ nullable: true })
    allowRead: boolean;

    @Column({ nullable: true })
    allowUpdate: boolean;

    @Column({ nullable: true })
    allowDelete: boolean;

    @Column({ nullable: true })
    allowExecute: boolean;

    @Column({ nullable: true })
    allowAll: boolean;


    /* --- ASSOCIATIONS --- */

    @ManyToOne(type => AuthzGroup, authzGroup => authzGroup.authzGroupAuthzs)
    authzGroup: AuthzGroup;    

    @ManyToOne(type => Authz, authz => authz.authzGroupAuthzs)
    authz: Authz;    
}