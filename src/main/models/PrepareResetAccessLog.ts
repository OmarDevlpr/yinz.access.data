import LogModel from "@yinz/commons.data/ts/LogModel";
import { Entity, Column } from "typeorm";

@Entity()
export default class PrepareResetAccessLog extends LogModel {

    @Column({ nullable: true })
    userCode: string;
    
    @Column({ nullable: true })
    resetAccessUrl: string;

}