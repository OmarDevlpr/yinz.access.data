import LogModel from "@yinz/commons.data/ts/LogModel";
import { Entity, Column } from "typeorm";

@Entity()
export default class PrepareSetupAccessLog extends LogModel {

    @Column({ nullable: true })
    email: string;

    @Column({ nullable: true })
    phone: string;

    @Column({ nullable: true })
    firstName: string;

    @Column({ nullable: true })
    lastName: string;

    @Column({ nullable: true })
    salutation: string;

    @Column({ nullable: true })
    languageCode: string;

    @Column({ nullable: true })
    setupAccessUrl: string;
    
}