import { Column, Entity, ManyToOne } from "typeorm";
import { Model } from "@yinz/commons.data";
import Profile from "./Profile";
import Authz from "./Authz";

@Entity()
export default class ProfileAuthz extends Model {
        
    @Column({ nullable: true })
    allowCreate: boolean;

    @Column({ nullable: true })
    allowRead: boolean;

    @Column({ nullable: true })
    allowUpdate: boolean;

    @Column({ nullable: true })
    allowDelete: boolean;

    @Column({ nullable: true })
    allowExecute: boolean;

    @Column({ nullable: true })
    allowAll: boolean;
    

    /* --- ASSOCIATIONS --- */

    @ManyToOne(type => Profile, profile => profile.profileAuthzs, {primary: true})
    profile: Profile;

    @ManyToOne(type => Authz, authz => authz.profileAuthzs, { primary: true })
    authz: Authz;

}