import LogModel from "@yinz/commons.data/ts/LogModel";
import { Entity, Column } from "typeorm";

@Entity()
export default class ChangePasswordLog extends LogModel {

    @Column({ nullable: true })
    reqUser: string;

    @Column({ nullable: true })
    oldPassword: string;

    @Column({ nullable: true })
    newPassword: string;
       
}