import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Model } from "@yinz/commons.data";
import ProfileAuthz from "./ProfileAuthz";
import AuthzGroupAuthz from "./AuthzGroupAuthz";

@Entity()
export default class Authz extends Model {

    /* --- ID --- */
    @PrimaryGeneratedColumn()
    id: number



    /* --- FIELDS --- */
    @Column({ unique: true })
    code: string;

    @Column({ nullable: true })
    name: string;
  
    /* --- AUDIT TRAIL FIELDS --- */
    @Column({ type: 'date', nullable: true })
    createdOn: Date;

    @Column({ nullable: true })
    createdBy: string;

    @Column({ type: 'date', nullable: true })
    lastUpdateOn: Date;

    @Column({ nullable: true })
    lastUpdateBy: string;



    /* --- ASSOCIATIONS --- */

    @OneToMany(type => ProfileAuthz, profileAuthzs => profileAuthzs.authz)
    profileAuthzs: ProfileAuthz[];

    @OneToMany(type => AuthzGroupAuthz, authzGroupAuthz => authzGroupAuthz.authz)
    authzGroupAuthzs: AuthzGroupAuthz[];
}
