// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import LogoutLog from "../../main/models/LogoutLog";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const logoutLogDao = container.getBean<TypeormDao<LogoutLog>>('logoutLogDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.LoginLogTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let lougoutLog = new LogoutLog();
        lougoutLog.reqUser = chance.string({ length: 10 });        

        let logoutLog2 = new LogoutLog();
        logoutLog2.reqUser = lougoutLog.reqUser
        

        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await logoutLogDao.create(trans, lougoutLog, options);
        let createAllResult = await logoutLogDao.createAll(trans, [logoutLog2], options);
        let updateByFilterResult = await logoutLogDao.updateByFilter(trans, { reqUser: createResult.reqUser }, { reqUser: createResult.reqUser }, options);
        let updateByIdResult = await logoutLogDao.updateById(trans, { reqUser: createResult.reqUser }, createResult.id, options);
        let findAllResult = await logoutLogDao.findAll(trans, options);
        let findByIdResult = await logoutLogDao.findById(trans, createResult.id, options);
        let findByFilterResult = await logoutLogDao.findByFilter(trans, { reqUser: createResult.reqUser }, options);
        let deleteByFilterResult = await logoutLogDao.deleteByFilter(trans, { reqUser: "dont" }, options);
        let deleteByIdREsult = await logoutLogDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 2)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });



});
