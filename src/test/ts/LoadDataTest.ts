import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";
import DataLoader from "@yinz/tools.dataloader/ts/DataLoader"
import { TypeormDataSource, TypeormDao } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Authz from "@yinz/access.data/models/Authz";
import AuthzGroup from "@yinz/access.data/models/AuthzGroup";
import AuthzGroupAuthz from "../../main/models/AuthzGroupAuthz";
import Profile from "../../main/models/Profile";
import User from "../../main/models/User";

const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/tools.dataloader/ts',
        process.cwd() + '/dist/main/ts'
    ],
});


const dataLoader = container.getBean<DataLoader>('dataLoader');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

const authzDao = container.getBean<TypeormDao<Authz>>('authzDao');
const authzGroupDao = container.getBean<TypeormDao<AuthzGroup>>('authzGroupDao');
const authzGroupAuthzDao = container.getBean<TypeormDao<AuthzGroupAuthz>>('authzGroupAuthzDao');
const profileDao = container.getBean<TypeormDao<Profile>>('profileDao');
const userDao = container.getBean<TypeormDao<User>>('userDao');

let conn: YinzConnection;
let trans: YinzTransaction;


describe('| access.access.loadData', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            dataLoader.resetCache()
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            dataLoader.resetCache()
            resolve(true);
        })
    });

    it('| should load authzes', async () => {

        // 1. test data
        const fileNames = './src/main/data/__module_data__.yml'
        const options = { user: "__super_user__" }

        // 2. exec test
        const result = await dataLoader.load(trans, fileNames, options)

        // 3. check result  
        // 3.1 check load result
        assert.ok(result);
        assert.strictEqual(Object.keys(result).length, 5);

        assert.ok(result.authz, stringify(result));
        assert.ok(result.authz.recs);
        assert.strictEqual(result.authz.recs.total, 14);
        assert.strictEqual(result.authz.recs.loaded, 14);

        assert.ok(result.authzGroup, stringify(result));
        assert.ok(result.authzGroup.recs);
        assert.strictEqual(result.authzGroup.recs.total, 3);
        assert.strictEqual(result.authzGroup.recs.loaded, 3);

        assert.ok(result.authzGroupAuthz, stringify(result));
        assert.ok(result.authzGroupAuthz.recs);
        assert.strictEqual(result.authzGroupAuthz.recs.total, 42);
        assert.strictEqual(result.authzGroupAuthz.recs.loaded, 42);

        assert.ok(result.profile, stringify(result));
        assert.ok(result.profile.recs);
        assert.strictEqual(result.profile.recs.total, 1);
        assert.strictEqual(result.profile.recs.loaded, 1);

        assert.ok(result.user, stringify(result));
        assert.ok(result.user.recs);
        assert.strictEqual(result.user.recs.total, 1);
        assert.strictEqual(result.user.recs.loaded, 1);



        // 3.2 check db records
        assert.strictEqual((await authzDao.findAll(trans, options)).length, 14)
        assert.strictEqual((await authzGroupDao.findAll(trans, options)).length, 3)
        assert.strictEqual((await authzGroupAuthzDao.findAll(trans, options)).length, 42)
        assert.strictEqual((await profileDao.findAll(trans, options)).length, 1)
        assert.strictEqual((await userDao.findAll(trans, options)).length, 1)






    });
});
