// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import User from "../../main/models/User";
import Profile from "../../main/models/Profile";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const userDao = container.getBean<TypeormDao<User>>('userDao');
const profileDao = container.getBean<TypeormDao<Profile>>('profileDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.UserTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let user = new User();        
        user.code   = chance.string({ length: 10 });
        user.status = "A";

        let user2 = new User();        
        user2.code = chance.string({ length: 10 });
        user2.status = "I";

        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult         = await userDao.create(trans, user, options);        
        let createAllResult      = await userDao.createAll(trans, [user2], options);
        let updateByFilterResult = await userDao.updateByFilter(trans, { status: createResult.status }, { status: createResult.status }, options);
        let updateByIdResult     = await userDao.updateById(trans, { status: createResult.status }, createResult.id, options);
        let findAllResult        = await userDao.findAll(trans, options);
        let findByIdResult       = await userDao.findById(trans, createResult.id, options);
        let findByFilterResult   = await userDao.findByFilter(trans, { status: createResult.status }, options);
        let deleteByFilterResult = await userDao.deleteByFilter(trans, { status: "L" }, options);
        let deleteByIdREsult     = await userDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 1)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });




    it('| # updateByFilter --> should execute successfully given that profile is in the updated fields ', async () => {

        // 1. prepare test-data 

        let profile = new Profile();
        profile.code = chance.string({ length: 10 });
        profile = await profileDao.create(trans, profile, { user: "__super_user__" });

        let user = new User();
        user.code = chance.string({ length: 10 });
        user.status = "A";                
        user.profiles = [profile];
        user = await userDao.create(trans, user, {user: "__super_user__", include: ["profiles"]});

        let profiles = [{
            ...profile,
            code: chance.string({length: 10})
        }]
        
        // 2. execute test        
        let updateResult = await userDao.updateByFilter(trans, { profiles }, {code: user.code}, {user: "__super_user__", include: ['profiles']});

        assert.ok(updateResult.data)        
        assert.strictEqual(updateResult.data.profiles[0].code, profiles[0].code)


    });

});
