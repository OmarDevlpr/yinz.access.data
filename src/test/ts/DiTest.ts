import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');


describe('| access.data.di', function () {
    it('| should assemble the module', function () {
        try {
            // classes
            assert.ok(container.getClazz('AuthzGroup'));
            assert.ok(container.getClazz('Profile'));
            assert.ok(container.getClazz('User'));
            assert.ok(container.getClazz('Authz'));
            assert.ok(container.getClazz('SetupAccess'));
            assert.ok(container.getClazz('SetupAccessLog'));
            assert.ok(container.getClazz('ResetAccessLog'));
            assert.ok(container.getClazz('AccessManager'));
            assert.ok(container.getClazz('PrepareResetAccessLog'));
            assert.ok(container.getClazz('PrepareSetupAccessLog'));
            assert.ok(container.getClazz('AuthzGroupAuthz'));
            assert.ok(container.getClazz('ProfileAuthz'));
            
            // beans
            assert.ok(container.getBean('authzGroupDao'));
            assert.ok(container.getBean('authzDao'));
            assert.ok(container.getBean('userDao'));
            assert.ok(container.getBean('profileDao'));
            assert.ok(container.getBean('loginLogDao'));
            assert.ok(container.getBean('setupAccessDao'));
            assert.ok(container.getBean('setupAccessLogDao'));
            assert.ok(container.getBean('resetAccessLogDao'));
            assert.ok(container.getBean('prepareResetAccessLogDao'));
            assert.ok(container.getBean('prepareSetupAccessLogDao'));
            assert.ok(container.getBean('authzGroupAuthzDao'));
            assert.ok(container.getBean('profileAuthzDao'));
            assert.ok(container.getBean('accessManager'));
            
        } catch (e) {
            if (e instanceof Exception) {
                assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
            }
            else {
                assert.ok(false, 'exception: ' + e.message + '\n' + e.stack);
            }
        }
    });
});
