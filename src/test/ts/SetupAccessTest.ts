// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import SetupAccess from "../../main/models/SetupAccess";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const setupAccessDao = container.getBean<TypeormDao<SetupAccess>>('setupAccessDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.SetupAccessTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let setupAccess = new SetupAccess();
        setupAccess.code     = chance.string({ length: 10 });        
        setupAccess.password = chance.string({ length: 10 });
        setupAccess.ownerId  = chance.integer({ min: 1, max: 20000 })
        setupAccess.status   = "W"

        let setupAccess2 = new SetupAccess();
        setupAccess2.code     = chance.string({ length: 10 });
        setupAccess2.password = chance.string({ length: 10 });
        setupAccess2.ownerId = chance.integer({ min: 1, max: 20000 })
        setupAccess2.status   = "W"
        

        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await setupAccessDao.create(trans, setupAccess, options);
        let createAllResult = await setupAccessDao.createAll(trans, [setupAccess2], options);
        let updateByFilterResult = await setupAccessDao.updateByFilter(trans, { password: createResult.password }, { password: createResult.password }, options);
        let updateByIdResult = await setupAccessDao.updateById(trans, { password: createResult.password }, createResult.id, options);
        let findAllResult = await setupAccessDao.findAll(trans, options);
        let findByIdResult = await setupAccessDao.findById(trans, createResult.id, options);
        let findByFilterResult = await setupAccessDao.findByFilter(trans, { password: createResult.password }, options);
        let deleteByFilterResult = await setupAccessDao.deleteByFilter(trans, { status: "A" }, options);
        let deleteByIdREsult = await setupAccessDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 1)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });

});
