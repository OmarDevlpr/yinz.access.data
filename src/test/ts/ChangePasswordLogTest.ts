// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import ChangePasswordLog from "../../main/models/ChangePasswordLog";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const changePasswordLogDao = container.getBean<TypeormDao<ChangePasswordLog>>('changePasswordLogDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.ChangePasswordLogTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let changePasswordLog = new ChangePasswordLog();
        changePasswordLog.reqUser = chance.string({ length: 10 });
        changePasswordLog.oldPassword = chance.string({ length: 10 });
        changePasswordLog.newPassword = chance.string({ length: 10 });

        let changePasswordLog2 = new ChangePasswordLog();
        changePasswordLog2.reqUser = changePasswordLog.reqUser
        changePasswordLog2.oldPassword = chance.string({ length: 10 });
        changePasswordLog2.newPassword = chance.string({ length: 10 });

        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await changePasswordLogDao.create(trans, changePasswordLog, options);
        let createAllResult = await changePasswordLogDao.createAll(trans, [changePasswordLog2], options);
        let updateByFilterResult = await changePasswordLogDao.updateByFilter(trans, { reqUser: createResult.reqUser }, { reqUser: createResult.reqUser }, options);
        let updateByIdResult = await changePasswordLogDao.updateById(trans, { reqUser: createResult.reqUser }, createResult.id, options);
        let findAllResult = await changePasswordLogDao.findAll(trans, options);
        let findByIdResult = await changePasswordLogDao.findById(trans, createResult.id, options);
        let findByFilterResult = await changePasswordLogDao.findByFilter(trans, { reqUser: createResult.reqUser }, options);
        let deleteByFilterResult = await changePasswordLogDao.deleteByFilter(trans, { reqUser: "dont" }, options);
        let deleteByIdREsult = await changePasswordLogDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 2)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });



});
