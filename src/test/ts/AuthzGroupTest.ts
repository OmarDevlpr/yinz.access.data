// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import AuthzGroup from "../../main/models/AuthzGroup";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const authzGroupDao = container.getBean<TypeormDao<AuthzGroup>>('authzGroupDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.AuthzGroupTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let authzGroup = new AuthzGroup();
        authzGroup.name = chance.string({ length: 10 });
        authzGroup.code = chance.string({ length: 10 });

        let authzGroup2 = new AuthzGroup();
        authzGroup2.name = authzGroup.name
        authzGroup2.code = chance.string({ length: 10 });

        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult         = await authzGroupDao.create(trans, authzGroup, options);        
        let createAllResult      = await authzGroupDao.createAll(trans, [authzGroup2], options);
        let updateByFilterResult = await authzGroupDao.updateByFilter(trans, { name: createResult.name }, { name: createResult.name }, options);
        let updateByIdResult     = await authzGroupDao.updateById(trans, { name: createResult.name }, createResult.id, options);
        let findAllResult        = await authzGroupDao.findAll(trans, options);
        let findByIdResult       = await authzGroupDao.findById(trans, createResult.id, options);
        let findByFilterResult   = await authzGroupDao.findByFilter(trans, { name: createResult.name }, options);
        let deleteByFilterResult = await authzGroupDao.deleteByFilter(trans, { name: "dont" }, options);
        let deleteByIdREsult     = await authzGroupDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)

        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 2)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });

});
