// libs
import * as assert from "assert";
import { Chance } from "chance";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
// import { Logger, YinzOptions, Exception } from "@yinz/commons";
import { TypeormDataSource, TypeormDao } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import AccessManager, { AuthzOpts } from "main/ts/AccessManager";
import Profile from "../../main/models/Profile";
import User from "../../main/models/User";
import Authz from "../../main/models/Authz";
import { Exception } from "@yinz/commons";
import AuthzGroup from "../../main/models/AuthzGroup";
import ProfileAuthz from "../../main/models/ProfileAuthz";
import AuthzGroupAuthz from "../../main/models/AuthzGroupAuthz";
// test dependencies



const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

// const logger = container.getBean<Logger>('logger');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');
const accessManager = container.getBean<AccessManager>('accessManager');

let conn: YinzConnection;
let trans: YinzTransaction;



let profileDao = container.getBean<TypeormDao<Profile>>('profileDao');
let profileAuthzDao = container.getBean<TypeormDao<ProfileAuthz>>('profileAuthzDao');
let authzGroupAuthzDao = container.getBean<TypeormDao<AuthzGroupAuthz>>('authzGroupAuthzDao');
let userDao = container.getBean<TypeormDao<User>>('userDao');
let authzDao = container.getBean<TypeormDao<Authz>>('authzDao');
let authzGroupDao = container.getBean<TypeormDao<AuthzGroup>>('authzGroupDao');

// let permissions = ['allowCreate', 'allowRead', 'allowUpdate', 'allowDelete', 'allowAll', 'allowExecute']

type ALLOW_CREATE = 'allowCreate';
type ALLOW_READ = 'allowRead';
type ALLOW_UPDATE = 'allowUpdate';
type ALLOW_DELETE = 'allowDelete';
type ALLOW_ALL = 'allowAll';
type ALLOW_EXECUTE = 'allowExecute';

type permissions = ALLOW_CREATE | ALLOW_READ | ALLOW_UPDATE | ALLOW_DELETE | ALLOW_ALL | ALLOW_EXECUTE;

const createUserWithAuthzGroup = async (
    authzPermissions: permissions,
    linkUserToProfile: boolean = true,
    linkProfileToAuthzGroup: boolean = true,
    linkAuthzGroupToAuthz: boolean = true)
    : Promise<{ user: User, authz: Authz, profile: Profile }> => {

    let user = new User();
    user.code = chance.string({ length: 10 });
    user.status = "A";

    let profile = new Profile();
    profile.code = chance.string({ length: 10 });

    let authz = new Authz();
    authz.code = chance.string({ length: 10, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' });

    let authzGroup = new AuthzGroup();
    authzGroup.code = chance.string({length: 10});

    authzGroup = await authzGroupDao.create(trans, authzGroup, { user: '__super_user__' });
    authz = await authzDao.create(trans, authz, { user: '__super_user__' });

    profile.authzGroups = [authzGroup];
    profile = await profileDao.create(trans, profile, { user: '__super_user__', include: ['authzGroups'] });

    // 1. link authzGroup to authz

    if ( linkAuthzGroupToAuthz ) {

        let authzGroupAuthz = new AuthzGroupAuthz();
        authzGroupAuthz.authz = authz;
        authzGroupAuthz.authzGroup = authzGroup;

        let permissions = Array.isArray(authzPermissions) ? authzPermissions : [authzPermissions];

        for (let permission of permissions) {
            authzGroupAuthz[permission] = true;
        }

        authzGroupAuthz = await authzGroupAuthzDao.create(trans, authzGroupAuthz, {user: "__super_user__", include: ['authz', 'authzGroup']});
    }

    // link authzGroup to profile
    if ( linkAuthzGroupToAuthz)
        profile.authzGroups = [authzGroup];
    
    // link user to profile
    if (linkUserToProfile)
        user.profiles = [profile];

    user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

    return { user, authz, profile };

};

const createUserWithAuthz = async (
     authzPermissions: permissions,
     linkUserToProfile: boolean = true,
     linkProfileToAuthz: boolean = true)
    : Promise<{user: User, authz: Authz, profile: Profile}> => {

    let user = new User();
    user.code = chance.string({ length: 10 });
    user.status = "A";

    let profile = new Profile();
    profile.code = chance.string({ length: 10 });

    let authz = new Authz();
    authz.code = chance.string({ length: 10, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' });


    authz = await authzDao.create(trans, authz, { user: '__super_user__' });
    profile = await profileDao.create(trans, profile, { user: '__super_user__' });

    if ( linkProfileToAuthz)     {
        let profileAuthz = new ProfileAuthz()        
        profileAuthz.authz = authz;
        profileAuthz.profile = profile;

        let permissions = Array.isArray(authzPermissions) ? authzPermissions : [authzPermissions];

        for (let permission of permissions ) {

            profileAuthz[permission] = true;

        }

        profileAuthz = await profileAuthzDao.create(trans, profileAuthz, { user: '__super_user__', include: ['profile', 'authz'] });
    }

    if ( linkUserToProfile)
        user.profiles = [profile];

    user = await userDao.create(trans, user, { include: ['profiles'], user: '__super_user__' });

    return {user, authz, profile};

}

describe('| access.data.AccessManager_isUserGrantedAuthzesTest ', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    describe('| #isUserGrantedAuthzes ', function () {
        


        beforeEach(() => {
            return new Promise(async (resolve) => {
                trans = await dataSource.startTrans(conn);                
                resolve(trans);
            })
        });

        afterEach(() => {
            return new Promise(async (resolve) => {
                await dataSource.rollbackTrans(trans)
                resolve(true);
            })
        });

        

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [create] with user who is allowed create', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthz("allowCreate");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'create'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );            
                      
        })


        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [any] with user who is allowed create', async () => {

            // 1. prepare test-data
    
            // 1.1 generate user
            let { user, authz, profile } = await createUserWithAuthz("allowCreate");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

         
            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );            

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [create] with super user', async () => {

            // 1. prepare test-data
            let requestedAuthz: AuthzOpts = {
                subject: chance.string({length: 10}),
                action: 'create'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, "__super_user__", requestedAuthz);


            // 3. compare result
            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);            
            assert.strictEqual(result.userCode, "__super_user__")

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [create] with user who is allowed All', async () => {

        // 1. prepare test-data
            
            // 1.1 generate user
            let { user, authz, profile } = await createUserWithAuthz("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'create'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

         
            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );            

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [create] with user who doesn\'t have a profile', async () => {

            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowCreate", false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'create'
            }            

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [create] with user who has a profile with no authz and not authz group', async () => {


            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowCreate", true, false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'create'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should throw Exception on action [create] with user who doesn\'t exist', async () => {

            // 1. prepare test-data
            
            let { authz } = await createUserWithAuthz("allowCreate");

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'create'
            }

            let wrongUserCode = chance.string({length: 10});

            try {
                await accessManager.isUserGrantedAuthzes(trans, wrongUserCode, requestedAuthz);
            } catch (e ) {

                if ( e instanceof Exception) {
                    assert.strictEqual(e.reason, "ER_ACCESS_MNG__LOOKUP_USER_AUTHZES__USER_NOT_FOUND")
                    assert.strictEqual(e.message, `The supplied user [${wrongUserCode}] does not exist!`)                    
                } else {
                    console.warn(e)
                    throw new Error("BAD FLOW... we should not be here!") 
                }
            }                        
        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [create] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data

            let {  user } = await createUserWithAuthz("allowAll");

            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'create'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })

        // /*********************************************************************************************************************
        // /*********************************************************************************************************************
        // /*********************************************************************************************************************
        // /********************************************************************************************************************/
         
         

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [read] with user who is allowed read', async () => {

              // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthz("allowRead");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );               

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [any] with user who is allowed read', async () => {

              // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthz("allowRead");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );           

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [read] with super user', async () => {

            // 1. prepare test-data
            let requestedAuthz: AuthzOpts = {
                subject: chance.string({ length: 10 }),
                action: 'read'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, "__super_user__", requestedAuthz);


            // 3. compare result
            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.strictEqual(result.userCode, "__super_user__")

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [read] with user who is allowed All', async () => {

          // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthz("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );           
        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [read] with user who doesn\'t have a profile', async () => {

            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowRead", false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [read] with user who has a profile with no authz and not authz group', async () => {

            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowRead", true, false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should throw Exception on action [read] with user who doesn\'t exist', async () => {

            // 1. prepare test-data

            let { authz } = await createUserWithAuthz("allowRead");

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }

            let wrongUserCode = chance.string({ length: 10 });

            try {
                await accessManager.isUserGrantedAuthzes(trans, wrongUserCode, requestedAuthz);
            } catch (e) {

                if (e instanceof Exception) {
                    assert.strictEqual(e.reason, "ER_ACCESS_MNG__LOOKUP_USER_AUTHZES__USER_NOT_FOUND")
                    assert.strictEqual(e.message, `The supplied user [${wrongUserCode}] does not exist!`)
                } else {
                    console.warn(e)
                    throw new Error("BAD FLOW... we should not be here!")
                }
            }   
        })

        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [read] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data

            let { user } = await createUserWithAuthz("allowAll");

            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'read'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        // /*********************************************************************************************************************
        // /*********************************************************************************************************************
        // /*********************************************************************************************************************
        // /********************************************************************************************************************/


        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [update] with user who is allowed update', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz, profile } = await createUserWithAuthz("allowUpdate");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'update'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.ok(result.profile);


            assert.strictEqual(result.subject, requestedAuthz.subject);
            assert.strictEqual(result.action, requestedAuthz.action);
            assert.strictEqual(result.userCode, user.code);
            assert.strictEqual(result.profile && result.profile.id, profile.id);
            assert.strictEqual(result.profile && result.profile.code, profile.code);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id, authz.id);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code, authz.code);           

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [any] with user who is allowed update', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz, profile } = await createUserWithAuthz("allowUpdate");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.ok(result.profile);


            assert.strictEqual(result.subject, requestedAuthz.subject);
            assert.strictEqual(result.action, requestedAuthz.action);
            assert.strictEqual(result.userCode, user.code);
            assert.strictEqual(result.profile && result.profile.id, profile.id);
            assert.strictEqual(result.profile && result.profile.code, profile.code);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id, authz.id);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code, authz.code);               

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [update] with super user', async () => {

            // 1. prepare test-data
            let requestedAuthz: AuthzOpts = {
                subject: chance.string({ length: 10 }),
                action: 'update'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, "__super_user__", requestedAuthz);


            // 3. compare result
            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.strictEqual(result.userCode, "__super_user__")

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [update] with user who is allowed All', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz, profile } = await createUserWithAuthz("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'update'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.ok(result.profile);


            assert.strictEqual(result.subject, requestedAuthz.subject);
            assert.strictEqual(result.action, requestedAuthz.action);
            assert.strictEqual(result.userCode, user.code);
            assert.strictEqual(result.profile && result.profile.id, profile.id);
            assert.strictEqual(result.profile && result.profile.code, profile.code);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id, authz.id);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code, authz.code);            

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [update] with user who doesn\'t have a profile', async () => {

            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowUpdate", false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [update] with user who has a profile with no authz and not authz group', async () => {

            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowUpdate", true, false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'update'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should throw Exception on action [update] with user who doesn\'t exist', async () => {

            // 1. prepare test-data

            let { authz } = await createUserWithAuthz("allowUpdate");

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'update'
            }

            let wrongUserCode = chance.string({ length: 10 });

            try {
                await accessManager.isUserGrantedAuthzes(trans, wrongUserCode, requestedAuthz);
            } catch (e) {

                if (e instanceof Exception) {
                    assert.strictEqual(e.reason, "ER_ACCESS_MNG__LOOKUP_USER_AUTHZES__USER_NOT_FOUND")
                    assert.strictEqual(e.message, `The supplied user [${wrongUserCode}] does not exist!`)
                } else {
                    console.warn(e)
                    throw new Error("BAD FLOW... we should not be here!")
                }
            }   
        })

        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [update] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data

            let { user } = await createUserWithAuthz("allowAll");

            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'update'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })

        // /****************************************************************************************************************************
        // /****************************************************************************************************************************
        // /****************************************************************************************************************************
        // /****************************************************************************************************************************
        // /***************************************************************************************************************************/        
         
        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [delete] with user who is allowed delete', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz, profile } = await createUserWithAuthz("allowDelete");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'delete'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.ok(result.profile);


            assert.strictEqual(result.subject, requestedAuthz.subject);
            assert.strictEqual(result.action, requestedAuthz.action);
            assert.strictEqual(result.userCode, user.code);
            assert.strictEqual(result.profile && result.profile.id, profile.id);
            assert.strictEqual(result.profile && result.profile.code, profile.code);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id, authz.id);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code, authz.code);             

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [any] with user who is allowed delete', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz, profile } = await createUserWithAuthz("allowDelete");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.ok(result.profile);


            assert.strictEqual(result.subject, requestedAuthz.subject);
            assert.strictEqual(result.action, requestedAuthz.action);
            assert.strictEqual(result.userCode, user.code);
            assert.strictEqual(result.profile && result.profile.id, profile.id);
            assert.strictEqual(result.profile && result.profile.code, profile.code);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id, authz.id);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code, authz.code);                    

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [delete] with super user', async () => {

            // 1. prepare test-data
            let requestedAuthz: AuthzOpts = {
                subject: chance.string({ length: 10 }),
                action: 'delete'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, "__super_user__", requestedAuthz);


            // 3. compare result
            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.strictEqual(result.userCode, "__super_user__")

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [delete] with user who is allowed All', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz, profile } = await createUserWithAuthz("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'delete'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.ok(result.profile);


            assert.strictEqual(result.subject, requestedAuthz.subject);
            assert.strictEqual(result.action, requestedAuthz.action);
            assert.strictEqual(result.userCode, user.code);
            assert.strictEqual(result.profile && result.profile.id, profile.id);
            assert.strictEqual(result.profile && result.profile.code, profile.code);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id, authz.id);
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code, authz.code);             

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [delete] with user who doesn\'t have a profile', async () => {

            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowUpdate", false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'delete'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [delete] with user who has a profile with no authz and not authz group', async () => {

            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowDelete", true, false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'delete'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should throw Exception on action [delete] with user who doesn\'t exist', async () => {

            // 1. prepare test-data

            let { authz } = await createUserWithAuthz("allowDelete");

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'delete'
            }

            let wrongUserCode = chance.string({ length: 10 });

            try {
                await accessManager.isUserGrantedAuthzes(trans, wrongUserCode, requestedAuthz);
            } catch (e) {

                if (e instanceof Exception) {
                    assert.strictEqual(e.reason, "ER_ACCESS_MNG__LOOKUP_USER_AUTHZES__USER_NOT_FOUND")
                    assert.strictEqual(e.message, `The supplied user [${wrongUserCode}] does not exist!`)
                } else {
                    console.warn(e)
                    throw new Error("BAD FLOW... we should not be here!")
                }
            }   
        })

        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [delete] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data

            let { user } = await createUserWithAuthz("allowAll");

            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'delete'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        // /*******************************************************************************************************************************
        // /*******************************************************************************************************************************
        // /*******************************************************************************************************************************
        // /*******************************************************************************************************************************
        // /******************************************************************************************************************************/

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [execute] with user who is allowed execute', async () => {

             // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthz("allowExecute");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'execute'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );         

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [any] with user who is allowed execute', async () => {

           // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthz("allowExecute");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );                  

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [execute] with super user', async () => {


            // 1. prepare test-data
            let requestedAuthz: AuthzOpts = {
                subject: chance.string({ length: 10 }),
                action: 'execute'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, "__super_user__", requestedAuthz);


            // 3. compare result
            assert.ok(result);
            assert.ok(result.subject);
            assert.ok(result.action);
            assert.ok(result.userCode);
            assert.strictEqual(result.userCode, "__super_user__")

        })

        it('| isUserGrantedAuthzes (authz level) --> should return [not null] on action [execute] with user who is allowed All', async () => {

             // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthz("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'execute'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                        , requestedAuthz.subject);
            assert.strictEqual(result.action                                                         , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                       , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                   , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                 , profile.code          );            
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.profileAuthzs[0].authz.code          , authz.code            );                    

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [execute] with user who doesn\'t have a profile', async () => {


            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowExecute", false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'execute'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [execute] with user who has a profile with no authz and not authz group', async () => {

            // 1. prepare test-data

            let { authz, user } = await createUserWithAuthz("allowExecute", true, false);

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'execute'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })


        it('| isUserGrantedAuthzes (authz level) --> should throw Exception on action [execute] with user who doesn\'t exist', async () => {

            // 1. prepare test-data

            let { authz } = await createUserWithAuthz("allowExecute");

            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'execute'
            }

            let wrongUserCode = chance.string({ length: 10 });

            try {
                await accessManager.isUserGrantedAuthzes(trans, wrongUserCode, requestedAuthz);
            } catch (e) {

                if (e instanceof Exception) {
                    assert.strictEqual(e.reason, "ER_ACCESS_MNG__LOOKUP_USER_AUTHZES__USER_NOT_FOUND")
                    assert.strictEqual(e.message, `The supplied user [${wrongUserCode}] does not exist!`)
                } else {
                    console.warn(e)
                    throw new Error("BAD FLOW... we should not be here!")
                }
            }   
        })
        
        it('| isUserGrantedAuthzes (authz level) --> should return [null] on action [execute] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data

            let { user } = await createUserWithAuthz("allowAll");

            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'execute'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare outcome
            assert.strictEqual(result, null)

        })

        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */
        // /***************************************************************************************************************************** */

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [create] with user who is allowed create', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowCreate");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'create'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );             

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [any] with user who is allowed create', async () => {

             // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowCreate");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );            

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [create] with user who is allowed All', async () => {

              // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'create'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );             

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [create] with user who has a profile with an authz group that has no authz', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz} = await createUserWithAuthzGroup("allowCreate", true, true, false);

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'create'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.strictEqual(result, null);
        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [create] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user } = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'create'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })

        // /************************************************************************************************************************************ */
        // /************************************************************************************************************************************ */
        // /************************************************************************************************************************************ */
        // /************************************************************************************************************************************ */
        // /************************************************************************************************************************************ */


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [read] with user who is allowed read', async () => {

           
            // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowRead");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );                      

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [any] with user who is allowed read', async () => {

                    // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowRead");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );          

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [read] with user who is allowed All', async () => {

                    // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );         

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [read] with user who has a profile with an authz group that has no authz', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz } = await createUserWithAuthzGroup("allowRead", true, true, false);

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'read'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [read] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user } = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'read'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })


        // /********************************************************************************************************************************** */
        // /********************************************************************************************************************************** */
        // /********************************************************************************************************************************** */
        // /********************************************************************************************************************************** */
        
        
        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [update] with user who is allowed update', async () => {

             // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowUpdate");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'update'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );              

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [any] with user who is allowed update', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowUpdate");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );         

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [update] with user who is allowed All', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'update'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );          

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [update] with user who has a profile with an authz group that has no authz', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz } = await createUserWithAuthzGroup("allowUpdate", true, true, false);

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'update'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [update] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user } = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'update'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })


        // /******************************************************************************************************************************************* */
        // /******************************************************************************************************************************************* */
        // /******************************************************************************************************************************************* */
        // /******************************************************************************************************************************************* */



        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [delete] with user who is allowed delete', async () => {

             // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowDelete");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'delete'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );          

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [any] with user who is allowed delete', async () => {

             // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowDelete");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );         

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [delete] with user who is allowed All', async () => {

             // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'delete'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );             

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [delete] with user who has a profile with an authz group that has no authz', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz } = await createUserWithAuthzGroup("allowDelete", true, true, false);

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'delete'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [delete] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user } = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'delete'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })


        // /********************************************************************************************************************************** */
        // /********************************************************************************************************************************** */
        // /********************************************************************************************************************************** */
        // /********************************************************************************************************************************** */


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [execute] with user who is allowed execute', async () => {

             // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowExecute");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'execute'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );         

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [any] with user who is allowed execute', async () => {

               // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowExecute");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'any'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );                 

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [not null] on action [execute] with user who is allowed All', async () => {

               // 1. prepare test-data            

            // 1.1 generate user
            let {user, authz, profile} = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'execute'
            }                                                                  

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);           

            // 3. compare result 

            assert.ok(result);
            assert.ok(result.subject );
            assert.ok(result.action  );
            assert.ok(result.userCode);
            assert.ok(result.profile );            
            
            
            assert.strictEqual(result.subject                                                                          , requestedAuthz.subject);
            assert.strictEqual(result.action                                                                           , requestedAuthz.action );
            assert.strictEqual(result.userCode                                                                         , user.code             );
            assert.strictEqual(result.profile && result.profile.id                                                     , profile.id            );
            assert.strictEqual(result.profile && result.profile.code                                                   , profile.code          );            
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.id            , authz.id              );
            assert.strictEqual(result.profile && result.profile.authzGroups[0].authzGroupAuthzs[0].authz.code          , authz.code            );     

        })


        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [execute] with user who has a profile with an authz group that has no authz', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user, authz } = await createUserWithAuthzGroup("allowExecute", true, true, false);

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: authz.code,
                action: 'execute'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })

        it('| isUserGrantedAuthzes (authzGroup level) --> should return [null] on action [execute] with user who allowed All where subject is empty string', async () => {

            // 1. prepare test-data            

            // 1.1 generate user
            let { user } = await createUserWithAuthzGroup("allowAll");

            // 1.2 requestedAuthz
            let requestedAuthz: AuthzOpts = {
                subject: "",
                action: 'execute'
            }

            // 2. execute test
            let result = await accessManager.isUserGrantedAuthzes(trans, user.code, requestedAuthz);

            // 3. compare result 

            assert.strictEqual(result, null);

        })
    })
  
});
