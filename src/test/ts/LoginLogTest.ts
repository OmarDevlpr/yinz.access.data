// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import LoginLog from "../../main/models/LoginLog";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const loginLogDao = container.getBean<TypeormDao<LoginLog>>('loginLogDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.LoginLogTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {

        // 1. prepare test-data 

        let loginLog = new LoginLog();
        loginLog.reqUser = chance.string({ length: 10 });
        loginLog.password = chance.string({ length: 10 });

        let loginLog2 = new LoginLog();
        loginLog2.reqUser = loginLog.reqUser
        loginLog2.password = chance.string({ length: 10 });

        let options = { user: "__super_user__" };
        // 2. execute test

        let createResult = await loginLogDao.create(trans, loginLog, options);
        let createAllResult = await loginLogDao.createAll(trans, [loginLog2], options);
        let updateByFilterResult = await loginLogDao.updateByFilter(trans, { reqUser: createResult.reqUser }, { reqUser: createResult.reqUser }, options);
        let updateByIdResult = await loginLogDao.updateById(trans, { reqUser: createResult.reqUser }, createResult.id, options);
        let findAllResult = await loginLogDao.findAll(trans, options);
        let findByIdResult = await loginLogDao.findById(trans, createResult.id, options);
        let findByFilterResult = await loginLogDao.findByFilter(trans, { reqUser: createResult.reqUser }, options);
        let deleteByFilterResult = await loginLogDao.deleteByFilter(trans, { reqUser: "dont" }, options);
        let deleteByIdREsult = await loginLogDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)
                
        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 2)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);

    });



});
