// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import Authz from "../../main/models/Authz";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import AuthzGroup from "../../main/models/AuthzGroup";
import AuthzGroupAuthz from "../../main/models/AuthzGroupAuthz";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const authzDao = container.getBean<TypeormDao<Authz>>('authzDao');
const authzGroupDao = container.getBean<TypeormDao<AuthzGroup>>('authzGroupDao');
const authzGroupAuthzDao = container.getBean<TypeormDao<AuthzGroupAuthz>>('authzGroupAuthzDao');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

let conn: YinzConnection;
let trans: YinzTransaction;



describe('| access.data.AuthzTest', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);            
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it('| # (all dao crud functions) --> should execute successfully ', async () => {
        
        // 1. prepare test-data 

        let authz = new Authz();
        authz.name = chance.string({ length: 10});
        authz.code = chance.string({ length: 10});

        let authz2 = new Authz();
        authz2.name = authz.name
        authz2.code = chance.string({ length: 10 });

        let options = { user: "__super_user__"};
        // 2. execute test

        let createResult         = await authzDao.create(trans, authz, options);        
        let createAllResult      = await authzDao.createAll(trans, [authz2], options);
        let updateByFilterResult = await authzDao.updateByFilter(trans, { name: createResult.name }, { name: createResult.name }, options);
        let updateByIdResult     = await authzDao.updateById(trans, { name: createResult.name }, createResult.id, options);
        let findAllResult        = await authzDao.findAll(trans, options);
        let findByIdResult       = await authzDao.findById(trans, createResult.id, options);
        let findByFilterResult   = await authzDao.findByFilter(trans, { name: createResult.name }, options);
        let deleteByFilterResult = await authzDao.deleteByFilter(trans, { name: "dont" }, options);
        let deleteByIdREsult     = await authzDao.deleteById(trans, createResult.id, options);

        // 3. compare result

        assert.ok(createResult)
        assert.ok(createResult.id)
        assert.ok(createResult.createdBy)
        assert.ok(createResult.createdOn)

        assert.ok(createAllResult)
        assert.ok(createAllResult[0].id)
        assert.ok(createAllResult[0].createdBy)
        assert.ok(createAllResult[0].createdOn)

        assert.ok(updateByFilterResult)

        assert.ok(updateByIdResult)
        
        assert.ok(findAllResult)
        assert.ok(findAllResult.length === 2)

        assert.ok(findByIdResult)
        assert.ok(findByIdResult.id === createResult.id)

        assert.ok(findByFilterResult)
        assert.ok(findByFilterResult.length === 2)

        assert.ok(deleteByFilterResult)
        assert.ok(deleteByFilterResult.meta)
        assert.ok(deleteByFilterResult.meta.affectedCount === 0)

        assert.ok(deleteByIdREsult)
        assert.ok(deleteByIdREsult.meta)
        assert.ok(deleteByIdREsult.meta.affectedCount === 1);
                        
    });


    it('| # should load authzGroupAuthz when provided in include ', async () => {

        let options = { user: "__super_user__" };

        // 1. prepare test-data 

        let authz = new Authz();
        authz.name = chance.string({ length: 10 });
        authz.code = chance.string({ length: 10 });
        authz = await authzDao.create(trans, authz, options);


        let authzGroup = new AuthzGroup()
        authzGroup.code = chance.string({ length: 10 });
        authzGroup.name = chance.string({ length: 10 });
        authzGroup = await authzGroupDao.create(trans, authz, options);
        
        let authzGroupAuthz = new AuthzGroupAuthz();
        authzGroupAuthz.allowAll = true;
        authzGroupAuthz.authz = authz;
        authzGroupAuthz.authzGroup = authzGroup;
        authzGroupAuthz = await authzGroupAuthzDao.create(trans, authzGroupAuthz, {...options, include: ['authz', 'authzGroup']})

        // 2 exec
     
        let result = await authzDao.findAll(trans, {...options, include: ['authzGroupAuthzs']});

        // 3. compare result

       assert.ok(result)

    });


});
